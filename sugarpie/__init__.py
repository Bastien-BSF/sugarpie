"""SugarPie is a Python library to drive the PiSugar UPS / Battery."""
from .pisugar import Pisugar

__version__ = "1.4.0"

pisugar = Pisugar()
